#!/usr/bin/env python3
# SPDX-License-Identifier: BSD-3-Clause
# Copyright 2023 Nicolas Dufresne <nicolas.dufresne@collabora.com>

import gi

gi.require_version('Gst', '1.0')
gi.require_version('GstApp', '1.0')

from gi.repository import GLib
from gi.repository import Gst
from gi.repository import GstApp

import matplotlib.pyplot as plt
import numpy as np
import argparse
import signal
import sys

class BitrateMonitor:
    RATE_PERIOD = 5 # seconds
    VIDEO_RATE_SEQUENCE = [ 5000, 3000, 6000, 4000]
    AUDIO_RATE_SEQUENCE = [ 64, 32, 128, 96]
    GOP_LENGTH = 30

    ENCODERS = {
    #   element name      type     to Kb/s  extra options 
        'v4l2slh264enc': ('video', 1000, f'keyframe-interval={GOP_LENGTH}'),
        'v4l2slvp8enc':  ('video', 1000, f'keyframe-interval={GOP_LENGTH}'),
        'x264enc':       ('video',    1, f'key-int-max={GOP_LENGTH} pass=cbr'),
        'opusenc':       ('audio', 1000, f'bitrate-type=cbr'),
    }

    def __init__(self, args):
        self.main_loop = GLib.MainLoop()
        self.time = []
        self.rate = []
        self.target = []
        self.filename = args.filename
        self.sequence_index = 0;

        cfg = self.ENCODERS[args.encoder]
        self.encoder = args.encoder
        self.encoder_type = cfg[0]
        self.bitrate_multiplier = cfg[1]
        self.encoder_opt = cfg[2]

        if self.encoder_type == 'video':
            self.rate_sequence = self.VIDEO_RATE_SEQUENCE
        else:
            self.rate_sequence = self.AUDIO_RATE_SEQUENCE

        self.bitrate = self.rate_sequence[self.sequence_index]

    def pull_sample(self):
        sample = self.appsink.try_pull_sample(40 * Gst.MSECOND)
        if sample:
            segment = sample.get_segment()
            buffer = sample.get_buffer()

            ts = segment.to_running_time(Gst.Format.TIME, buffer.pts)
            self.time.append(ts / Gst.SECOND)
            # kbits/s
            self.rate.append(buffer.get_size() * 8 * Gst.SECOND /
                    buffer.duration / 1000)
            self.target.append(self.bitrate)

            if self.time[-1] >= self.RATE_PERIOD * (self.sequence_index + 1):
                self.sequence_index += 1

                if self.sequence_index >= len(self.rate_sequence):
                    self.stop()
                    return GLib.SOURCE_REMOVE

                print(f"Sequence {self.sequence_index} at rate {self.bitrate} Kb/s")
                self.bitrate = self.rate_sequence[self.sequence_index]
                self.encoder.props.bitrate = self.bitrate * self.bitrate_multiplier

        return GLib.SOURCE_CONTINUE


    def run(self):
        if self.encoder_type == 'video':
            source = f"videotestsrc pattern=snow ! video/x-raw,width=320,height=240"
        else:
            source = f"audiotestsrc wave=white-noise"
        encoder = f"{self.encoder} name=encoder {self.encoder_opt}"
        appsink = f"appsink name=sink sync=0"

        self.pipeline = Gst.parse_launch(" ! ".join([source, encoder, appsink]))
        self.encoder = self.pipeline.get_by_name("encoder")
        self.encoder.props.bitrate = self.bitrate * self.bitrate_multiplier
        self.appsink = self.pipeline.get_by_name("sink")

        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect("message::eos", self.eos)

        print(f"Starting encoder test with rate {self.bitrate} Kb/s")
        self.pipeline.set_state(Gst.State.PLAYING)
        GLib.idle_add (self.pull_sample)

        self.main_loop.run()


    def stop(self):
        self.pipeline.set_state(Gst.State.NULL)
        self.main_loop.quit()

        # Data for plotting
        fig, ax = plt.subplots()
        ax.plot(self.time, self.rate, label='Effective Rate', color='b')
        ax.plot(self.time, self.target, label='Target Rate', color='r')

        ax.set(xlabel='Time (seconds)', ylabel='Rate (Kb/s)',
            title='Encoder Data Rate')
        ax.grid()
        fig.savefig(self.filename)

    def signal(self, source, condition):
        self.stop()

    def eos(self, pspec, msg):
        self.stop()



if __name__ == "__main__":
    Gst.init(None)

    parser = argparse.ArgumentParser(
                    prog='bitrate-monitor',
                    description='Program to test rate control on gstreamer encoders.',
                    epilog='bitrate-monitor -e v4l2slh264dec out.png')
    parser.add_argument('-e', '--encoder',
                        default='v4l2slh264enc',
                        choices=[
                            'v4l2slh264enc', 'v4l2slvp8enc', 'x264enc',
                            'opusenc'])
    parser.add_argument('filename')
    args = parser.parse_args()

    monitor = BitrateMonitor(args)
    signal.signal(signal.SIGINT, monitor.signal)
    monitor.run()

    print("\nBye.")
    sys.exit(0)
